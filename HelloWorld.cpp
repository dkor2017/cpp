#include <iostream>

class Animal 
{
public:
	virtual void Voice()
	{
		std::cout << "Animal voice"<<'\n';
	}
	Animal() {};
};
class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!" << '\n';
	}
	Cat() {};
};
class Goose : public Animal
{
public:
	void Voice() override
	{
		std::cout << "GA!" << '\n';
	}
	Goose() {};
};
class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Mow!" << '\n';
	}
	Cow() {};
};
class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!" << '\n';
	}
};

int main()
{
	Animal** animals = new Animal*[4];
	animals[0] = new Dog;
	animals[1] = new Cat;
	animals[2] = new Cow;
	animals[3] = new Goose;

	for (int i = 0; i < 4; i++)
	{
		animals[i]->Voice();
	}
	

	return 0;
}